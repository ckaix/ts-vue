pipeline{
    agent any
    environment {
        cacheDir = "stage" //定义缓存的目录名字
        cachePackage = "${cacheDir}/package.json" //定义缓存的package.json
        cacheCommitIDFile = "${cacheDir}/.commitIDCache.txt" //把成功打包的commitID缓存到这里
        artifactsDir = "${cacheDir}/artifacts" //制品缓存的目录，构建成功的制品我们放这里
        resetFlagFile = "${cacheDir}/.resetFile" //回滚标志的文件
        cacheCommitIDMax = 5         //缓存版本的最大值
    }
    options {
      disableConcurrentBuilds()       // 在多分支的流水线中限制并发
    }
    stages{
        stage("pre-build"){
            when {
              anyOf {
                branch 'dev'
                branch 'release'
                branch 'master'
              }
            }
            agent {
              docker {
                image "node:10.21.0"
                // 想在 docker 容器中运行代码，但是也想使用流水线所定义的相同节点和工作空间，必须设置这个
                reuseNode true
              }
            }
            steps{
                sh "printenv"
                sh "chmod +x ./jenkins/script/pre-build.sh"
                sh "./jenkins/script/pre-build.sh"
            }
        }
        stage("build-env"){
            when {
              anyOf {
                branch 'dev'
                branch 'release'
                branch 'master'
              }
            }
            failFast false
            parallel {
              stage("build-dev"){
                  when {
                    // beforeAgent 是指在进入agent ，如果when的条件对，才进入，错则不进入
                    // 就是可以加快流水线的运行啦
                    beforeAgent true
                    branch 'dev'
                  }
                  agent {
                    docker {
                      image "node:10.21.0"
                      // 想在 docker 容器中运行代码，但是也想使用流水线所定义的相同节点和工作空间，必须设置这个
                      reuseNode true
                    }
                  }
                  steps{
                    sh "chmod +x ./jenkins/script/build-dev.sh"
                    sh "./jenkins/script/build-dev.sh"
                  }
              }
              stage("build-release"){
                  when {
                    // beforeAgent 是指在进入agent ，如果when的条件对，才进入，错则不进入
                    // 就是可以加快流水线的运行啦
                    beforeAgent true
                    branch 'release'
                  }
                  agent {
                    docker {
                      image "node:10.21.0"
                      // 想在 docker 容器中运行代码，但是也想使用流水线所定义的相同节点和工作空间，必须设置这个
                      reuseNode true
                    }
                  }
                  steps{
                    sh "chmod +x ./jenkins/script/build-release.sh"
                    sh "./jenkins/script/build-release.sh"
                  }
              }
              stage("build-master"){
                  when {
                    // beforeAgent 是指在进入agent ，如果when的条件对，才进入，错则不进入
                    // 就是可以加快流水线的运行啦
                    beforeAgent true
                    branch 'master'
                  }
                  agent {
                    docker {
                      image "node:10.21.0"
                      // 想在 docker 容器中运行代码，但是也想使用流水线所定义的相同节点和工作空间，必须设置这个
                      reuseNode true
                    }
                  }
                  steps{
                    sh "chmod +x ./jenkins/script/build-prod.sh"
                    sh "./jenkins/script/build-prod.sh"
                  }
              }
            }
        }
        stage("artifacts-manage"){
            steps {
              echo "artifacts"
            }
        }
        stage('deliver') {
            when {
                beforeAgent true
                anyOf {
                    branch 'dev'
                    branch 'release'
                }
            }
            steps {
                echo "start deliver"
            }
        }
        stage('deploy') {
            when {
                beforeAgent true
                branch 'master'
            }
            steps {
                // 这个就是生成一个按钮，我们用来手动发布的
                input message: "Should you deploy?"
                echo "start deloy"
            }
        }
    }
    post{
        changed{
            echo 'I changed!'
        }
        failure{
            echo 'I failed!'
        }
        success{
            echo 'I success'
        }
        always{
            echo 'I always'
        }
        unstable{
            echo "unstable"
        }
        aborted{
            echo "aborted"
        }
    }
}
