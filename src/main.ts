import { Component } from "vue-property-decorator";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
// import { authService } from "auth-ckaix";
// eslint-disable-next-line @typescript-eslint/no-var-requires
// const { authService } = require("auth-ckaix/dist/bundle.cjs.js");
import { authService } from "auth-ckaix";
console.log(authService);

Component.registerHooks([
  "beforeRouteEnter",
  "beforeRouteLeave",
  "beforeRouteUpdate"
]);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
