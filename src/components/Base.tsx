import { Component, Vue } from "vue-property-decorator";
import Props from "./Props";
@Component({
  components: {}
})
export default class Home extends Vue {
  render() {
    return (
      <div>
        <Props />
      </div>
    );
  }
}
