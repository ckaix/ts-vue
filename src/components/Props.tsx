import { Component, Vue, Prop } from "vue-property-decorator";

@Component({
  components: {}
})
export default class Home extends Vue {
  @Prop({
    required: true,
    type: String
  })
  msg!: string;
  render() {
    return <div>{this.msg}</div>;
  }
}
